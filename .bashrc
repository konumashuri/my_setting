# .bashrc
alias ls='ls -i -l'
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias vi='vim'
alias vs="vagrant ssh"
alias be="bundle exec"
alias fs="bundle exec foreman start"
alias mysql="mysql -u root -p"

alias stg.coromos='ssh -i ~/uiscope.pem ec2-user@stg.coromos.com'
alias mstr.coromos='ssh -i ~/uiscope.pem ec2-user@www.coromos.com'
alias tool='ssh -i ~/uiscope.pem ec2-user@54.250.143.161'
alias stg.uiscope='ssh -i ~/uiscope.pem ec2-user@54.238.234.51'
alias uiscope_1='ssh -i ~/uiscope.pem ec2-user@54.238.229.241'
alias uiscope_2='ssh -i ~/uiscope.pem ec2-user@54.199.225.116'
alias uiscope_admin='ssh -i ~/uiscope.pem ec2-user@54.238.233.137'

source Library/git-completion.bash

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions


#show git branch in the terminal prompt
parse_git_branch(){
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\] $ "
