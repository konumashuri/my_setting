"----------------------------------------------------------------
" neobundle plugin manager
set nocompatible               " Be iMproved
filetype off		       " Required!


if has('vim_starting')
endif

set runtimepath+=~/.vim/bundle/neobundle.vim/
call neobundle#begin(expand('~/.vim/bundle/'))

filetype plugin indent on "Required!


"Installation check.
if neobundle#exists_not_installed_bundles()
	echomsg 'Not installed bundles :'.
		\ string (neobundle#get_not_installe_bundle_names())
	echomsg 'Please execute ":NeoBundleInstall" command.'
endif

" My Bundles here:
NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'Shougo/neosnippet-snippets'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'ggreer/the_silver_searcher'
NeoBundle 'tpope/vim-rails'
NeoBundle 'scrooloose/syntastic'

" You can specify revision/branch/tag.
NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

"vim alias的な
nmap t [Tag]
"close tab
map <silent> [Tag]x :tabclose<CR>
"次のtab
map <silent> [Tag]n :tabnext<CR>
"前のtab
map <silent> [Tag]p :tabprevious<CR>

"indent 設定
set tabstop=2
set softtabstop=2
set shiftwidth=2

"color schema
syntax on

"find highlight
set hlsearch

"ctrlpの検索を無視するやつ
set wildignore+=*/tmp/*,*.so,*.swp,*.zip

".swpファイルを作らせない
set noswapfile

"行数表示
set number

"tabを空白に変換
:set expandtab
:retab 2

"単語補完
set completeopt=menuone
for k in split("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_",'\zs')
  exec "imap <expr> " . k . " pumvisible() ? '" . k . "' : '" . k . "\<C-X>\<C-P>\<C-N>'"
endfor

"backspaceの挙動
set backspace=start,eol
