-- MySQL dump 10.13  Distrib 5.6.15, for osx10.9 (x86_64)
--
-- Host: localhost    Database: innobeta_test
-- ------------------------------------------------------
-- Server version	5.6.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `added_fares`
--

DROP TABLE IF EXISTS `added_fares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `added_fares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `added_fare` int(11) DEFAULT NULL,
  `added_section` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `added_fares`
--

LOCK TABLES `added_fares` WRITE;
/*!40000 ALTER TABLE `added_fares` DISABLE KEYS */;
/*!40000 ALTER TABLE `added_fares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_digest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `information` text COLLATE utf8_unicode_ci,
  `fee` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (1,'konuma',NULL,'850','300','2014-02-14 09:13:30','2014-02-14 09:13:30');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monthly_payments`
--

DROP TABLE IF EXISTS `monthly_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthly_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `monthly_payment` bigint(20) DEFAULT NULL,
  `monthly_basic_fare` bigint(20) DEFAULT NULL,
  `monthly_added_fare` bigint(20) DEFAULT NULL,
  `monthly_total_payment` bigint(20) DEFAULT NULL,
  `settlement` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monthly_payments`
--

LOCK TABLES `monthly_payments` WRITE;
/*!40000 ALTER TABLE `monthly_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `monthly_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20140214090456'),('20140214092633'),('20140217080858'),('20140217082619'),('20140217084014'),('20140224083948'),('20140224084353'),('20140224084846'),('20140224084913'),('20140224085357'),('20140224085408'),('20140224102121'),('20140224102158'),('20140224102856'),('20140224102917'),('20140224104531'),('20140317053247'),('20140317054342'),('20140317055041');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_digest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hourly_payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `basic_fare` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `basic_section` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2a$10$vOaGXXp4CFrFptqsO6st5.5xnYsLqJgmaVQEH6.sCZPeZXvgm8OdS','1000','1000','2014-02-17 10:59:41','2014-03-07 09:05:30','渋谷〜小田原'),(2,'test','$2a$10$8D3KahVdSrTvyIXIzqmmIu0sdtkg8WqgzCBGn/AnkvcgKr1RUYh.O','1000','1000','2014-02-18 10:09:33','2014-03-07 09:05:48','渋谷〜小田原'),(11,'井野米太','$2a$10$3mYxCVzm3KMv/cYet7MOReCXOZnlwOwCRPePj6XVQ/ws08fjqMuei','850','400','2014-02-24 11:08:29','2014-03-07 09:05:03','渋谷〜小田原'),(14,'hoge','$2a$10$b7H888vamb5HvRh7KtOM2ua576XDsAw/k03D7eowUfIGXsrBWmoZK','850','400','2014-03-11 11:40:41','2014-03-11 11:40:41','渋谷〜小田原'),(15,'a','$2a$10$aSbCSRbOTEPbLv2sY53vkODpcMQGnyKAK4oEvm5aTqfN8CV6wn93m','850','400','2014-03-18 13:47:50','2014-03-18 13:47:50','渋谷〜小田原'),(16,'b','$2a$10$lSwzsR9.CKqfyen1PlgGj.URgnCNomiNxQfBzMYy0Y6f.joHsCNSS','850','400','2014-03-19 04:12:32','2014-03-19 04:12:32','渋谷〜小田原'),(17,'c','$2a$10$ISXV5ZBRZ0SLDJ4q7D0NVeBsVS6CquRrXffHK.04Z4MEIqTkmlLT.','850','500','2014-03-19 04:15:02','2014-03-19 04:15:02','渋谷〜小田原'),(18,'d','$2a$10$0r7SSfzCg8.U3AP8Nxneo.qBqvvsopUNZ9mfPD3rSmiYuruAzjtsC','d','d','2014-03-19 04:31:39','2014-03-19 04:31:39','d'),(19,'e','$2a$10$DmBTwSwyWdC70ok1iggtK.tKwImufgA4rVZA73H7PvpmoQoCUiwMm','e','e','2014-03-19 04:32:12','2014-03-19 04:32:12','e'),(20,'f','$2a$10$.EDzltvlAK.RX2uVMvYcaurPDbDJz2Zu5A6fCPBW1ipEBC.DSHNvS','f','f','2014-03-19 04:33:01','2014-03-19 04:33:01','f'),(21,'aa','$2a$10$pefYeaWbf/htLYZ5sik0Ee9Utcq7eMwJVT2d17LevD0J2wgTxElUe','aa','aa','2014-03-19 04:34:41','2014-03-19 04:34:41','aa');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worktimes`
--

DROP TABLE IF EXISTS `worktimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worktimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `coming` datetime DEFAULT NULL,
  `going` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worktimes`
--

LOCK TABLES `worktimes` WRITE;
/*!40000 ALTER TABLE `worktimes` DISABLE KEYS */;
INSERT INTO `worktimes` VALUES (3,NULL,'0012-02-14 00:00:00',NULL,'2014-02-14 10:42:48','2014-02-14 10:42:48'),(4,NULL,NULL,NULL,'2014-02-14 10:43:19','2014-02-14 10:43:19'),(5,NULL,'0012-02-14 00:00:00',NULL,'2014-02-14 10:43:49','2014-02-14 10:43:49'),(6,NULL,NULL,NULL,'2014-02-14 10:46:37','2014-02-14 10:46:37'),(7,NULL,NULL,NULL,'2014-02-14 10:48:00','2014-02-14 10:48:00'),(8,NULL,NULL,NULL,'2014-02-14 10:48:11','2014-02-14 10:48:11'),(9,NULL,'2014-02-14 10:48:00',NULL,'2014-02-14 10:53:31','2014-02-14 10:53:31'),(24,2,'2014-02-18 10:09:00','2014-02-18 10:09:00','2014-02-18 10:17:10','2014-02-18 10:17:10'),(26,2,'2014-02-18 12:56:00','2014-02-18 12:56:00','2014-02-18 13:00:41','2014-02-18 13:00:41'),(27,2,'2014-02-18 12:56:00','2014-02-18 12:56:00','2014-02-18 13:02:55','2014-02-18 13:02:55'),(28,2,'2014-02-18 12:56:00','2014-02-18 12:56:00','2014-02-18 13:03:01','2014-02-18 13:03:01'),(29,2,'2014-02-18 12:56:00','2014-02-18 12:56:00','2014-02-18 13:04:04','2014-02-18 13:04:04'),(30,2,'2014-02-18 12:56:00','2014-02-18 12:56:00','2014-02-18 13:04:53','2014-02-18 13:04:53'),(31,2,'2014-02-18 12:56:00','2014-02-18 12:56:00','2014-02-18 13:07:01','2014-02-18 13:07:01'),(32,2,'2014-02-18 12:56:00','2014-02-18 12:56:00','2014-02-18 13:07:17','2014-02-18 13:07:17'),(33,2,'2014-02-18 13:07:00','2014-02-18 13:07:00','2014-02-18 13:07:44','2014-02-18 13:07:44'),(34,2,'2014-02-18 13:07:00','2014-02-18 13:07:00','2014-02-18 13:07:54','2014-02-18 13:07:54'),(35,2,'2014-02-18 13:07:00','2014-02-18 13:07:00','2014-02-18 13:08:08','2014-02-18 13:08:08'),(36,2,'2014-02-18 13:14:00','2014-02-18 13:14:00','2014-02-18 13:14:11','2014-02-18 13:14:11'),(43,10,'2012-02-21 10:24:00','2019-02-21 10:24:00','2014-02-21 10:26:56','2014-02-22 06:25:25'),(47,2,'2014-02-22 08:37:00','2014-02-22 08:37:00','2014-02-22 08:37:31','2014-02-22 08:37:31'),(59,12,'2014-02-26 06:31:00','2014-02-26 07:07:00','2014-02-26 06:32:09','2014-02-26 07:07:15'),(63,6,'2014-02-26 00:00:00','2014-02-26 23:59:00','2014-02-26 07:20:51','2014-02-26 07:21:08'),(64,1,'2014-02-26 12:19:00','2014-02-26 16:19:00','2014-02-26 12:19:18','2014-02-26 12:19:45'),(67,6,'2014-02-27 10:21:00','2014-02-27 21:52:00','2014-02-27 03:21:11','2014-02-27 03:22:29'),(68,1,'2014-02-27 03:27:00','2014-02-27 03:30:00','2014-02-27 03:27:13','2014-02-27 03:30:51'),(69,2,'2014-02-26 03:37:00',NULL,'2014-02-27 03:37:20','2014-02-27 03:37:20'),(70,2,'2014-02-26 03:38:00',NULL,'2014-02-27 03:38:02','2014-02-27 03:38:02'),(71,2,'2014-02-27 12:50:00','2014-02-27 22:50:00','2014-02-27 03:50:38','2014-02-27 04:03:58'),(72,13,'2014-02-27 13:10:00','2014-02-27 21:40:00','2014-02-27 04:09:36','2014-02-27 04:09:50'),(73,12,'2014-02-27 06:12:00','2014-02-27 16:12:00','2014-02-27 06:12:22','2014-02-27 06:12:42'),(74,1,'2014-03-04 09:59:00',NULL,'2014-03-04 10:00:16','2014-03-04 10:00:16'),(75,1,'2014-03-05 06:28:00',NULL,'2014-03-05 06:28:23','2014-03-05 06:28:23'),(76,1,'2014-03-05 06:30:00','2014-03-05 06:30:00','2014-03-05 06:30:39','2014-03-05 06:30:39'),(77,1,'2014-03-07 03:00:00','2014-03-07 08:57:00','2014-03-07 08:57:37','2014-03-07 08:59:11'),(78,1,'2014-03-11 11:28:00','2014-03-11 11:28:00','2014-03-11 11:28:28','2014-03-11 11:28:49'),(79,1,'2014-03-15 01:16:00',NULL,'2014-03-18 05:22:46','2014-03-18 05:22:46'),(80,1,'2014-03-11 05:22:00',NULL,'2014-03-18 05:22:59','2014-03-18 05:22:59'),(82,NULL,NULL,NULL,'2014-03-18 08:34:35','2014-03-18 08:34:35'),(83,NULL,NULL,NULL,'2014-03-18 08:36:10','2014-03-18 08:36:10'),(84,NULL,NULL,NULL,'2014-03-18 09:21:38','2014-03-18 09:21:38'),(85,NULL,NULL,NULL,'2014-03-18 09:43:48','2014-03-18 09:43:48'),(86,NULL,NULL,NULL,'2014-03-18 10:00:38','2014-03-18 10:00:38'),(87,NULL,NULL,NULL,'2014-03-18 10:02:19','2014-03-18 10:02:19'),(88,NULL,NULL,NULL,'2014-03-18 10:02:48','2014-03-18 10:02:48'),(89,NULL,NULL,NULL,'2014-03-18 10:03:00','2014-03-18 10:03:00'),(90,NULL,NULL,NULL,'2014-03-18 12:18:23','2014-03-18 12:18:23'),(91,NULL,NULL,NULL,'2014-03-18 12:22:36','2014-03-18 12:22:36'),(92,NULL,NULL,NULL,'2014-03-18 12:23:04','2014-03-18 12:23:04'),(93,NULL,NULL,NULL,'2014-03-18 12:27:36','2014-03-18 12:27:36'),(94,NULL,NULL,NULL,'2014-03-18 12:34:07','2014-03-18 12:34:07'),(95,NULL,NULL,NULL,'2014-03-18 12:47:02','2014-03-18 12:47:02'),(96,NULL,NULL,NULL,'2014-03-18 12:58:02','2014-03-18 12:58:02'),(97,NULL,NULL,NULL,'2014-03-18 13:02:19','2014-03-18 13:02:19'),(98,NULL,NULL,NULL,'2014-03-18 13:03:14','2014-03-18 13:03:14'),(99,2,'2014-03-20 20:33:00',NULL,'2014-03-18 13:05:52','2014-03-18 13:05:52'),(100,2,'2014-03-18 13:07:14',NULL,'2014-03-18 13:07:16','2014-03-18 13:07:16'),(101,1,'2014-03-18 13:32:19','2014-03-18 13:45:00','2014-03-18 13:32:21','2014-03-18 13:45:17'),(102,15,'2014-03-18 13:47:54','2014-03-18 14:07:00','2014-03-18 13:47:58','2014-03-18 14:07:36'),(103,15,'2014-03-19 03:18:21','2014-03-19 03:31:00','2014-03-19 03:18:23','2014-03-19 03:31:54'),(104,1,'2014-03-19 04:10:17','2014-03-19 13:10:00','2014-03-19 04:10:19','2014-03-19 04:10:29'),(105,2,'2014-03-19 04:10:53','2014-03-19 13:10:00','2014-03-19 04:10:56','2014-03-19 04:11:04'),(106,16,'2014-03-19 04:12:37','2014-03-19 10:13:00','2014-03-19 04:12:59','2014-03-19 04:13:08'),(107,17,'2014-03-19 04:15:06','2014-03-19 09:15:00','2014-03-19 04:15:09','2014-03-19 04:15:22'),(108,18,'2014-03-19 04:31:42','2014-03-19 08:31:00','2014-03-19 04:31:46','2014-03-19 04:31:54'),(109,19,'2014-03-19 04:32:17','2014-03-19 12:32:00','2014-03-19 04:32:19','2014-03-19 04:32:26'),(110,20,'2014-03-19 04:33:03','2014-03-19 08:33:00','2014-03-19 04:33:06','2014-03-19 04:33:22'),(111,21,'2014-03-19 04:34:44','2014-03-19 09:34:00','2014-03-19 04:34:47','2014-03-19 04:34:53'),(113,11,NULL,NULL,'2014-03-22 08:39:53','2014-03-22 08:39:53'),(114,17,NULL,NULL,'2014-03-22 08:41:09','2014-03-22 08:41:09'),(115,NULL,NULL,NULL,'2014-03-22 08:42:00','2014-03-22 08:42:00'),(116,2,'2014-03-24 11:48:18',NULL,'2014-03-24 11:48:18','2014-03-24 11:48:18'),(117,14,'2014-03-28 11:52:01',NULL,'2014-03-28 11:52:01','2014-03-28 11:52:01'),(118,15,'2014-03-24 11:55:18',NULL,'2014-03-24 11:55:18','2014-03-24 11:55:18'),(119,NULL,'2014-04-03 12:59:03',NULL,'2014-04-03 12:59:03','2014-04-03 12:59:03'),(120,NULL,'2014-04-03 13:00:26',NULL,'2014-04-03 13:00:26','2014-04-03 13:00:26'),(121,NULL,'2014-04-03 13:09:25',NULL,'2014-04-03 13:09:25','2014-04-03 13:09:25'),(122,NULL,'2014-04-03 13:12:05',NULL,'2014-04-03 13:12:05','2014-04-03 13:12:05'),(123,NULL,'2014-04-03 13:12:25',NULL,'2014-04-03 13:12:25','2014-04-03 13:12:25'),(124,NULL,'2014-04-03 13:12:25',NULL,'2014-04-03 13:12:25','2014-04-03 13:12:25'),(125,2,'2014-04-03 13:17:31',NULL,'2014-04-03 13:17:31','2014-04-03 13:17:31'),(126,NULL,'2014-04-03 13:27:31',NULL,'2014-04-03 13:27:31','2014-04-03 13:27:31'),(127,NULL,'2014-04-03 13:27:55',NULL,'2014-04-03 13:27:55','2014-04-03 13:27:55'),(128,NULL,'2014-04-03 13:34:38',NULL,'2014-04-03 13:34:38','2014-04-03 13:34:38'),(129,NULL,'2014-04-03 13:37:41',NULL,'2014-04-03 13:37:41','2014-04-03 13:37:41'),(130,NULL,'2014-04-03 13:38:07','2014-04-03 13:38:00','2014-04-03 13:38:07','2014-04-03 13:38:07'),(131,NULL,'2014-04-03 13:38:55','2014-04-03 13:38:00','2014-04-03 13:38:55','2014-04-03 13:38:55'),(132,NULL,'2014-04-03 13:39:40','2014-04-03 13:39:00','2014-04-03 13:39:40','2014-04-03 13:39:40');
/*!40000 ALTER TABLE `worktimes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-16 23:07:04
